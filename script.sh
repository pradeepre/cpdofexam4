a_url="http://localhost:8081/artifactory"

repo="libs-snapshot-local"
artifact="com/agiletestingalliance/cpdofwebapp/CPDOFWebAppExam3"           

### Combine artifactory URL + repo name + artifact path
url=$a_url/$repo/$artifact

version=`curl -s $url/maven-metadata.xml | grep latest | sed "s/.*<latest>\([^<]*\)<\/latest>.*/\1/"`

build=`curl -s $url/$version/maven-metadata.xml | grep '<value>' |head -1 | sed "s/.*<value>\([^<]*\)<\/value>.*/\1/"`

LATEST_BUILD="$url/$version/CPDOFWebAppExam3-$build.war"
echo "File Name  = " +$LATEST_BUILD

echo $LATEST_BUILD > filename.txt
