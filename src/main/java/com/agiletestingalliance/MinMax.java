package com.agiletestingalliance;

public class MinMax {

    public int check(int first, int second) {
        if (second > first) {
            return second;
	}
        else {
            return first;
	}
    }

}
