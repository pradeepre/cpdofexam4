package com.agiletestingalliance;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.io.*;
import javax.servlet.http.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.mockito.Mockito;

public class MinMaxTest extends Mockito{

    @Test
    public void testMinMax() throws Exception {

        int k= new MinMax().check(6,5);
        assertEquals("MinMax",k,6);
        
    }
    
    @Test
    public void testMinMax1() throws Exception {

        int l= new MinMax().check(6,7);
        assertEquals("MinMax",l,7);

    }
   
}
